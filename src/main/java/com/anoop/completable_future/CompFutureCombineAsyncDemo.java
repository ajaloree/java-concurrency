package com.anoop.completable_future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.anoop.Utils.*;

/**
 * An ordinary future is typically created with a Callable, which is run, and
 * the result is obtained with a get()
 * @author ajaloree
 *
 */
public class CompFutureCombineAsyncDemo {
	
	public static void main(String[] args) throws Exception {
		
		ExecutorService ex = Executors.newFixedThreadPool(4);
		
		CompletableFuture<Double> cf1 = new CompletableFuture<>();
		CompletableFuture<Double> cf2 = new CompletableFuture<>();
		CompletableFuture<Double> cf3 = cf1.thenCombineAsync(cf2, (i,j)->Math.pow(i+j, 2));
		CompletableFuture<Double> cf4 = cf3.thenApplyAsync(v -> v*3);
		
		ex.submit(() -> cf1.complete(fx()));
		ex.submit(() -> cf2.complete(gx()));
		
		System.out.println("Result: "+cf4.get());
		
		shutdownAndAwaitTermination(ex);
		
	}
	private static Double fx() {
		System.out.println("Computing fx()....");
		simulateBusy((int)(10000*Math.random()));
		return 100*Math.random();
	}

	private static Double gx() {
		System.out.println("Computing gx()....");
		simulateBusy((int)(10000*Math.random()));
		return 100000*Math.random();
	}

}
