package com.anoop.callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class SimpleCallableDemo {
	
	public static void main(String[] args) throws Exception {
		
		ExecutorService exec = null;
		try {
			exec = Executors.newFixedThreadPool(5);
			Callable<Integer> c1 = sumFrom1to(500);
			Callable<Integer> c2 = sumFrom1to(500);
			
			Future<Integer> f1 = exec.submit(c1); //Callable execution starts at this point
			Future<Integer> f2 = exec.submit(c2); //and NOT when get() is called on Future.
			
			System.out.println("Tasks submitted - now waiting for them to finish....");
			System.out.println("Result-1: "+f1.get());
			System.out.println("Result-2: "+f2.get());
			
		} finally {
			if(exec!=null) exec.shutdown();
		}
		if(exec != null) {
			exec.awaitTermination(1, TimeUnit.MINUTES);
			if(exec.isTerminated()) {
				System.out.println("All tasks finished!");
			} else {
				System.out.println("Atleast one task is still running.");
			}
		}
	}
	
	private static Callable<Integer> sumFrom1to(int n) {
		Callable<Integer> callable = () -> {
			int sum = 0;
			for(int i=1; i <= n; i++) {
				sum++;
				if(i%100==0) {
					System.out.println(Thread.currentThread().getName()+" computing .....");
				}
			}
			return sum;
		};
		return callable;
	}
	

}
