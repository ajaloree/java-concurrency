package com.anoop.read_write_lock;

public class Reader implements Runnable {

	private final MutableResource<String, String> resource;
	
	public Reader(MutableResource<String, String> resource) {
		this.resource = resource;
	}
	
	@Override
	public void run() {
		
		while(true) {
			for(String key : resource.getKeys()) {
				System.out.println("Key: "+key+", Value: "+resource.get(key));
			}
		}
	}
}