package com.anoop.read_write_lock;

public class ReadWriteLockDemo {

	public static void main(String[] args) throws Exception {
		
		MutableResource<String, String> mr = new MutableResource<>();
		Thread[] readers = new Thread[4];
		for(int i = 1; i <= 4; i++) {
			readers[i-1] = new Thread(new Reader(mr), "Reader-"+i);
		}
		Thread writer = new Thread(new Writer(mr), "Writer");
		
		writer.start();
		for(Thread r : readers) {
			r.start();
		}
		
	}
	
}
