package com.anoop.read_write_lock;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.anoop.Utils.*;

public class MutableResource<K,V> {

	private final Map<K,V> cache = new ConcurrentHashMap<>();
	private final ReadWriteLock rwl = new ReentrantReadWriteLock();
	private final Lock rl = rwl.readLock();
	private final Lock wl = rwl.writeLock();
	
	public void add(K k, V v) {
		System.out.println("Write lock requested by thread "+Thread.currentThread().getName());
		wl.lock();
		try {
			System.out.println("Write lock acquired by thread "+Thread.currentThread().getName());
			simulateBusy(5000);
			cache.put(k, v);
		} finally {
			wl.unlock();
			System.out.println("Write lock released by thread "+Thread.currentThread().getName());
		}
	}
	
	public V get(K k) {
		System.out.println("Read lock requested by thread "+Thread.currentThread().getName());
		rl.lock();
		try {
			System.out.println("Read lock acquired by thread "+Thread.currentThread().getName());
			simulateBusy(2000);
			return cache.get(k);
		} finally {
			rl.unlock();
			System.out.println("Read lock released by thread "+Thread.currentThread().getName());
		}
	}

	public Set<K> getKeys() {
		System.out.println("Read lock requested by thread "+Thread.currentThread().getName());
		rl.lock();
		try {
			System.out.println("Read lock acquired by thread "+Thread.currentThread().getName());
			return cache.keySet();
		} finally {
			rl.unlock();
			System.out.println("Read lock released by thread "+Thread.currentThread().getName());
		}
	}

}
