package com.anoop.read_write_lock;

public class Writer implements Runnable {

	private final MutableResource<String, String> resource;
	
	public Writer(MutableResource<String, String> resource) {
		this.resource = resource;
	}
	
	@Override
	public void run() {
		
		while(true) {
			resource.add("K"+(int)(Math.random()*10000), "V"+(int)Math.random()*10000);
		}
	}
}