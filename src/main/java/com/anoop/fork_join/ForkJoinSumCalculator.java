package com.anoop.fork_join;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

@SuppressWarnings("serial")
public class ForkJoinSumCalculator extends RecursiveTask<Long> {

	// The array of numbers to be summed
	private final long[] numbers;
	// The initial and final positions of the subarray processed by this subtask
	private final int start;
	private final int end;
	// The size threshold to split into subtasks 
	public static final long THRESHOLD = 10_000;
	
	// Public constructor to create the main task
	public ForkJoinSumCalculator(long[] numbers) {
	    this(numbers, 0, numbers.length);
	}
	
	// Private constructor to create sub-task of the main task
	private ForkJoinSumCalculator(long[] numbers, int start, int end) {
	    this.numbers = numbers;
	    this.start = start;
	    this.end = end;
	}

	// Override the abstract method of the RecursiveTask
	@Override
	protected Long compute() {
		
		// The size of the sub-array summed by this recursive task
		int length = end - start;
		if(length <= THRESHOLD) {
			// If the size is less than or equal to the threshold, computes the result sequentially
			return computeSequentially();
		}
		// Creates a subtask to sum the first half of the array
		ForkJoinSumCalculator leftTask = new ForkJoinSumCalculator(numbers, start, start + length/2);
		// Asynchronously executes the newly created subtask using another thread of ForkJoinPool
		leftTask.fork();
		// Creates a subtask to sum the second half of the array
		ForkJoinSumCalculator rightTask = new ForkJoinSumCalculator(numbers, start + length/2, end);
		// Executes this second subtask synchronously, potentially allowing further recursive splits
		Long rightResult = rightTask.compute();
		// Reads the result of the first subtask—waiting if it isn’t ready
		Long leftResult = leftTask.join();
		// Combines the results of the two subtasks
		return leftResult + rightResult;
	}

	private long computeSequentially() {
		long sum = 0;
		for(int i = start; i < end; i++) {
			sum += numbers[i];
		}
		return sum;
	}
	
	// Driver function
	public static long forkJoinSum(long n) {
	    long[] numbers = LongStream.rangeClosed(1, n).toArray();
	    ForkJoinTask<Long> task = new ForkJoinSumCalculator(numbers);
	    return new ForkJoinPool().invoke(task);
	}
}