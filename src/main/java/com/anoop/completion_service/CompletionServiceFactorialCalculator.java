package com.anoop.completion_service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.anoop.Utils;
import com.anoop.future_task.FactorialWorker;

public class CompletionServiceFactorialCalculator {

	public Long factorial(long n) {
		
		int NUM_WORKERS = 4;
		long result = 1;

		ExecutorService exec = null;
		try {
			exec = Executors.newFixedThreadPool(NUM_WORKERS);
			CompletionService<Long> cs = new ExecutorCompletionService<>(exec);
			List<Callable<Long>> tasks = splitFactorialComputation(n, NUM_WORKERS);
			for(Callable<Long> task : tasks) {
				cs.submit(task);
			}
			
			for(int i = 0; i < tasks.size(); i++) {
				Future<Long> res = cs.take();
				result *= res.get();
			}
			
		} catch(Exception ex) {
			result = -1;
		} finally {
			Utils.shutdownAndAwaitTermination(exec);
		}
		return result;

	}
	
	private List<Callable<Long>> splitFactorialComputation(long number, int workers) {
		
		long size = number/workers;
		List<Callable<Long>> splits = new ArrayList<>();
		long i = 0;
		while(i < number) {
			
			long lo = i + 1;
			long hi = lo + size -1;
			if(hi >= number) {
				hi = number;
			}
			i = hi;
			splits.add(new FactorialWorker(lo, hi));
		}
		return splits;
	}
	
}
