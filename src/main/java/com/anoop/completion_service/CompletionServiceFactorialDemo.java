package com.anoop.completion_service;

public class CompletionServiceFactorialDemo {

	public static void main(String[] args) {
		
		CompletionServiceFactorialCalculator fc = new CompletionServiceFactorialCalculator();
		long res = fc.factorial(5);
		System.out.println(res);
	}
	
}
