package com.anoop;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class Utils {

	public static void simulateBusy(int ms) {
		try {
			Thread.sleep(ms);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void shutdownAndAwaitTermination(ExecutorService pool) {
		try {
			pool.shutdown();
			pool.awaitTermination(1, TimeUnit.MINUTES);
			if(pool.isTerminated()) {
				System.out.println("All tasks finished!");
			} else {
				System.out.println("Atleast one task is still running.");
				pool.shutdownNow();
			}
		} catch(Exception ex) {
			System.err.println("Error while shutting down executor service instance!");
		}
	}
	
	public static String getCurrThreadName() {
		return Thread.currentThread().getName();
	}

}
