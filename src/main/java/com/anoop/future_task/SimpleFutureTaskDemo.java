package com.anoop.future_task;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
/**
 * A Callable is submitted to an executor and a Future
 * object is returned. This future object is then polled using get()
 * for results. 
 * 
 * FutureTask wraps the Callable directly before submission
 * and there is no need to work with intermediate Future objects to poll
 * for results. It also offers flexibility to override functionalities
 * within protected methods to customise the task.
 */

public class SimpleFutureTaskDemo {
	
	public static void main(String[] args) throws Exception {
		
		ExecutorService exec = null;
		try {
			exec = Executors.newFixedThreadPool(5);

			FutureTask<Integer> f1 = new FutureTask<>(sumFrom1to(500));
			FutureTask<Integer> f2 = new FutureTask<>(sumFrom1to(500));
			
			exec.submit(f1); //Callable execution starts at this point
			exec.submit(f2); //and NOT when get() is called on Future.
			
			System.out.println("Tasks submitted - now waiting for them to finish....");
			System.out.println("Result-1: "+f1.get());
			System.out.println("Result-2: "+f2.get());
			
		} finally {
			if(exec!=null) exec.shutdown();
		}
		if(exec != null) {
			exec.awaitTermination(1, TimeUnit.MINUTES);
			if(exec.isTerminated()) {
				System.out.println("All tasks finished!");
			} else {
				System.out.println("Atleast one task is still running.");
			}
		}
	}
	
	private static Callable<Integer> sumFrom1to(int n) {
		Callable<Integer> callable = () -> {
			int sum = 0;
			for(int i=1; i <= n; i++) {
				sum++;
				if(i%100==0) {
					System.out.println(Thread.currentThread().getName()+" computing .....");
				}
			}
			return sum;
		};
		return callable;
	}
	

}
