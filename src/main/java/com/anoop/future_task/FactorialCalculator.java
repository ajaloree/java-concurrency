package com.anoop.future_task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import com.anoop.Utils;

public class FactorialCalculator {

	public Long factorial(long n) {
		
		int NUM_WORKERS = 4;
		long result = 1;

		ExecutorService exec = null;
		try {
			exec = Executors.newFixedThreadPool(NUM_WORKERS);
			List<FutureTask<Long>> tasks = splitFactorialComputation(n, NUM_WORKERS);
			for(FutureTask<Long> task : tasks) {
				exec.submit(task);
			}
			
			for(FutureTask<Long> task : tasks) {
				result *= task.get();
			}
			
		} catch(Exception ex) {
			result = -1;
		} finally {
			Utils.shutdownAndAwaitTermination(exec);
		}
		return result;

	}
	
	private List<FutureTask<Long>> splitFactorialComputation(long number, int workers) {
		
		long size = number/workers;
		List<FutureTask<Long>> splits = new ArrayList<>();
		long i = 0;
		while(i < number) {
			
			long lo = i + 1;
			long hi = lo + size -1;
			if(hi >= number) {
				hi = number;
			}
			i = hi;
			FactorialWorker worker = new FactorialWorker(lo, hi);
			splits.add(new FutureTask<>(worker));
		}
		return splits;
	}
	
}
