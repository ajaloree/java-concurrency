package com.anoop.future_task;

public class FactorialDemo {

	public static void main(String[] args) {
		
		FactorialCalculator fc = new FactorialCalculator();
		long res = fc.factorial(5);
		System.out.println(res);
	}
	
}
