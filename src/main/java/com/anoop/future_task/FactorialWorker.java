package com.anoop.future_task;

import java.util.concurrent.Callable;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FactorialWorker implements Callable<Long> {

	private final long from;
	private final long to;
	
	public Long call() throws Exception {
		long result = 1;
		for(long i = from; i <= to; i++) {
			result *= i;
		}
		System.out.println(Thread.currentThread().getName()+" computed product from "+from+" - to "+to+" as "+result);
		return result;
	}

}