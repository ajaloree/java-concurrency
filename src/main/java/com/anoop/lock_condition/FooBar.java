package com.anoop.lock_condition;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FooBar {
	
	private final int n;
	private final Lock lock;
	private final Condition fooCondition;
	private final Condition barCondition;
	private final AtomicInteger count;
	
	public FooBar(int n) {
		this.n = n;
		this.lock = new ReentrantLock();
		this.fooCondition = lock.newCondition();
		this.barCondition = lock.newCondition();
		this.count = new AtomicInteger(2);
	}
	
	public void foo() {
		lock.lock();
		try {
			for (int i = 0; i < n; i++) { 
				if(count.get()%2 != 0) {
					fooCondition.await();
				}
				System.out.print("foo");
				count.incrementAndGet();
				barCondition.signal();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			lock.unlock();
		}
	}	
	
	public void bar() {
		lock.lock();
		try {
			for (int i = 0; i < n; i++) { 
				if(count.get()%2 == 0) {
					barCondition.await();
				}
				System.out.print("bar");
				count.incrementAndGet();
				fooCondition.signal();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		} finally {
			lock.unlock();
		}
	}

}