package com.anoop.suspendable_thread;

public class SuspendableThreadDemo {

	public static void main(String[] args) throws Exception {
		
		SuspendableThread job1 = new SuspendableThread();
		SuspendableThread job2 = new SuspendableThread();

		(new Thread(job1)).start();
		(new Thread(job2)).start();
				
		Thread.sleep(5000);
		
		job1.suspend();
		job2.suspend();
		
		Thread.sleep(5000);
		
		job1.resume();
		//job2 remains suspended
	}
}
