package com.anoop.suspendable_thread;

import static com.anoop.Utils.*;

public class SuspendableThread implements Runnable {

	private volatile boolean suspend = false;
	private int count = 0;
	
	@Override
	public void run() {
		while(true) {
			try {
				if(suspend) {
					synchronized (this) {
						System.out.println(getCurrThreadName()+" thread suspended...");
						wait();
					}
				}
				System.out.println(getCurrThreadName()+" number produced: "+count++);
				simulateBusy(1000);
			} catch(Exception ex) {
				
			}
		}
	}
	
	public void resume() {
		suspend = false;
		synchronized (this) {
			System.out.println(getCurrThreadName()+" thread resumed...");
			notify();
		}
	}
	
	public void suspend() {
		suspend = true;
	}
	
}
