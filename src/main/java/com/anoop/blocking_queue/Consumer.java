package com.anoop.blocking_queue;

import com.anoop.Utils;

public class Consumer implements Runnable {
	
	private final BlockingJobQueue<Integer> queue;
	
	public Consumer(BlockingJobQueue<Integer> queue) {
		this.queue = queue;
	}
	
	@Override
	public void run() {
		while(true) {
			
			try {
				Utils.simulateBusy(10000);
				Integer i = queue.get();
				System.out.println("[Consumer] Received value "+i+" from the queue");
			} catch (InterruptedException e) {
				System.err.println("[Consumer] Error while reading from the queue!");
			}
		}
		
	}

}
