package com.anoop.blocking_queue;

public class BlockingQueueMain {

	public static void main(String[] args) throws Exception {
		
		//BlockingJobQueue<Integer> q = new WaitNotifyJobQueue<>(5);
		BlockingJobQueue<Integer> q = new LockConditionJobQueue<>(5);

		Runnable p = new Producer(q);
		Runnable c = new Consumer(q);
		Thread t1 = new Thread(p);
		Thread t2 = new Thread(c);
		t1.start();
		t2.start();
		t1.join();
		t2.join();
	}
}
