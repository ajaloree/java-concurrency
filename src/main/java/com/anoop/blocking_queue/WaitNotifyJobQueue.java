package com.anoop.blocking_queue;

import java.util.ArrayList;
import java.util.List;

public class WaitNotifyJobQueue<V> implements BlockingJobQueue<V> {
	
	private final int capacity;
	private final List<V> queue;
	
	public WaitNotifyJobQueue(int capacity) {
		this.capacity = capacity;
		queue = new ArrayList<>(capacity);
	}
	
	@Override
	public synchronized void put(V v) throws InterruptedException {
		if(queue.size() >= capacity) {
			System.out.println("Queue blocked as it's capacity is full!");
			wait();
		}
		queue.add(v);
		notify();
	}

	@Override
	public synchronized V get() throws InterruptedException {
		if(queue.isEmpty()) {
			System.out.println("Queue blocked as it's empty!");
			wait();
		}
		notify();
		return queue.get(0);
	}

}
