package com.anoop.blocking_queue;

public interface BlockingJobQueue<V> {

	public void put(V v) throws InterruptedException;
	
	public V get() throws InterruptedException;
	
}
