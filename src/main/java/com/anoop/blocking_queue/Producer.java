package com.anoop.blocking_queue;

import com.anoop.Utils;

public class Producer implements Runnable {
	
	private final BlockingJobQueue<Integer> queue;
	
	public Producer(BlockingJobQueue<Integer> queue) {
		this.queue = queue;
	}
	
	@Override
	public void run() {
		while(true) {
			
			try {
				Utils.simulateBusy(1000);
				Integer i = (int)(Math.random()*1000); 
				System.out.println("[Producer] Adding value "+i+" in the queue");
				queue.put(i);
			} catch (InterruptedException e) {
				System.err.println("[Producer] Error while writing to the queue!");
			}
		}
		
	}

}
