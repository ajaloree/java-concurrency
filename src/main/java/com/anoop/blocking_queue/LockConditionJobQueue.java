package com.anoop.blocking_queue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockConditionJobQueue<V> implements BlockingJobQueue<V> {
	
	private final int capacity;
	private final List<V> queue;
	private final Lock lock = new ReentrantLock();
	private final Condition jobConsumed = lock.newCondition();
	private final Condition jobAdded = lock.newCondition();
	
	public LockConditionJobQueue(int capacity) {
		this.capacity = capacity;
		queue = new ArrayList<>(capacity);
	}
	
	@Override
	public void put(V v) throws InterruptedException {
		
		lock.tryLock();
		try {
			if(queue.size() >= capacity) {
				System.out.println("Queue blocked as it's capacity is full!");
				jobConsumed.await();
			}
			queue.add(v);
			jobAdded.signal();
		} finally {
			lock.unlock();
		}
	}

	@Override
	public V get() throws InterruptedException {
		
		lock.tryLock();
		try {
			if(queue.isEmpty()) {
				System.out.println("Queue blocked as it's empty!");
				jobAdded.await();
			}
			jobConsumed.signal();
			return queue.get(0);
		} finally {
			lock.unlock();
		}
	}

}
