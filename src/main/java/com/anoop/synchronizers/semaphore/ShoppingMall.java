package com.anoop.synchronizers.semaphore;

import java.util.concurrent.Semaphore;

public class ShoppingMall {

	private final String name;
	private final Semaphore sem;
	
	public ShoppingMall(String name, int capacity) {
		this.name = name;
		this.sem = new Semaphore(capacity);
	}
	
	public void requestEntry() throws Exception {
		System.out.println("Customer "+Thread.currentThread().getName()+" requesting entry in "+name);
		sem.acquire();
		System.out.println("Customer "+Thread.currentThread().getName()+" granted access in "+name);
	}
	
	public void customerExit() throws Exception {
		sem.release();
		System.out.println("Customer "+Thread.currentThread().getName()+" exited "+name);
	}
	
}
