package com.anoop.synchronizers.semaphore;

import java.util.concurrent.Callable;

import static com.anoop.Utils.*;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Shopper implements Callable<Integer> {

	private final ShoppingMall mall;
	
	@Override
	public Integer call()throws Exception {
		
		mall.requestEntry();
		Integer num = (int)(Math.random()*10000);
		simulateBusy(num);
		System.out.println(Thread.currentThread().getName()+" - shopping done - spent £"+num+"!");
		mall.customerExit();
		return num;
	}

}
