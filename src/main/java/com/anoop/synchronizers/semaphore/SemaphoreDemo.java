package com.anoop.synchronizers.semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.anoop.Utils.*;

public class SemaphoreDemo {

	public static void main(String[] args) {
		
		
		ShoppingMall mall = new ShoppingMall("Westfield", 5);
		ExecutorService es = Executors.newFixedThreadPool(20);
		
		for(int i = 1; i <= 20; i++) {
			es.submit(new Shopper(mall));
		}
		
		shutdownAndAwaitTermination(es);
	}

}
