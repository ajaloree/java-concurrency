package com.anoop.synchronizers.count_down_latch;

import java.util.concurrent.CountDownLatch;

import static com.anoop.Utils.*;

import lombok.RequiredArgsConstructor;

/**
 * 
 * @author ajaloree
 *
 */
@RequiredArgsConstructor
public class Worker implements Runnable {
	
	// Start signal that prevents any worker from proceeding until the driver is ready for them to proceed;
	private final CountDownLatch startSignal;
	// A completion signal that allows the driver to wait until all workers have completed.
	private final CountDownLatch doneSignal;
	
	
	@Override
	public void run() {
		try {
			System.out.println(getCurrThreadName()+" awaiting for start signal ...");
			startSignal.await();
			System.out.println(getCurrThreadName()+" received start signal. Working ...");
			simulateBusy((int)(Math.random()*10000));
			System.out.println(getCurrThreadName()+" finished working.");
			doneSignal.countDown();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}
}
