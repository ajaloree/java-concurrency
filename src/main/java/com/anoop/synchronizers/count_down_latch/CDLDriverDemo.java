package com.anoop.synchronizers.count_down_latch;

import java.util.concurrent.CountDownLatch;

public class CDLDriverDemo {
	
	public static void main(String[] args) throws Exception {
		
		int N = 5;
		CountDownLatch startSignal = new CountDownLatch(1);
		CountDownLatch doneSignal = new CountDownLatch(N);
		
		for(int i = 1; i <= N; i++) {
			new Thread(new Worker(startSignal, doneSignal)).start();
		}
		
		System.out.println("All workers created - waiting for 5 seconds to give all of them a green signal...");
		Thread.sleep(5000);
		
		startSignal.countDown();
		
		System.out.println("Waiting for all threads to finish their work...");
		
		doneSignal.await();
		
		System.out.println("All done!");
	}
}