package com.anoop.synchronizers.cyclic_barrier;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FruitBasket {

	private Map<String, Integer> basket = new ConcurrentHashMap<>();
	
	public void incrementCountForItemBy(String item, Integer incr) {
		basket.put(item, basket.getOrDefault(item, 0)+incr);
	}
	
}
