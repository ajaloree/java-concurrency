package com.anoop.synchronizers.cyclic_barrier;

import com.anoop.Utils;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FruitBasketPacker implements Runnable {

	private final FruitBasket basket;	
	
	@Override
	public void run() {
		System.out.println("Packing basket with cellophane sheet...");
		Utils.simulateBusy(3000);
		System.out.println("Basket packed and ready to be shipped!");
	}

}
