package com.anoop.synchronizers.cyclic_barrier;

import java.util.concurrent.CyclicBarrier;

public class FruitBasketMakerDemo {
	
	public static void main(String[] args) {
		
		FruitBasket basket = new FruitBasket();

		CyclicBarrier barrier = new CyclicBarrier(3, new FruitBasketPacker(basket));

		FruitAdder fa1 = new FruitAdder("apple",5,barrier,basket);
		FruitAdder fa2 = new FruitAdder("orange",7,barrier,basket);
		FruitAdder fa3 = new FruitAdder("grape",15,barrier,basket);
		
		(new Thread(fa1)).start();
		(new Thread(fa2)).start();
		(new Thread(fa3)).start();

	}
	

}
