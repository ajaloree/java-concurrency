package com.anoop.synchronizers.cyclic_barrier;

import java.util.concurrent.CyclicBarrier;

import static com.anoop.Utils.*;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class FruitAdder implements Runnable {

	private String fruitName;
	private int total;
	private CyclicBarrier barrier;
	private FruitBasket basket;
	
	@Override
	public void run() {
		try {
			for(int i = 1; i <= total; i++) {
				basket.incrementCountForItemBy(fruitName, i);
				simulateBusy((int)(Math.random()*1000));
				System.out.println(fruitName+" added in the basket.");
			}
			System.out.println("Total "+total+" "+fruitName+"s added in the basket.");
			barrier.await();
		} catch(Exception ex) {
			
		}
		
	}

}
