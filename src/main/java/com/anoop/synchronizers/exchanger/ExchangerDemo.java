package com.anoop.synchronizers.exchanger;

import java.util.concurrent.Exchanger;

public class ExchangerDemo {

	public static void main(String[] args) throws Exception {

		Payload p1 = new Payload("Payload-1");
		Payload p2 = new Payload("Payload-2");
		
		Exchanger<Payload> exchanger = new Exchanger<>();
		
		Thread t1 = new Thread(new ExchangeWorker(exchanger,p1));
		Thread t2 = new Thread(new ExchangeWorker(exchanger,p2));
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join(); 
		
		
	}

}
