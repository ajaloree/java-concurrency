package com.anoop.synchronizers.exchanger;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@RequiredArgsConstructor
@ToString
public class Payload {

	private final String name;
	
}
