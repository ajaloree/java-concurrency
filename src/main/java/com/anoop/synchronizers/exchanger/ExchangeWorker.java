package com.anoop.synchronizers.exchanger;

import java.util.concurrent.Exchanger;

import static com.anoop.Utils.*;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ExchangeWorker implements Runnable {

	private final Exchanger<Payload> ex;
	private Payload p;
	
	public void run() {
		while(true) {
			try {
				System.out.println(getCurrThreadName()+" working with payload "+p);
				simulateBusy((int)(Math.random()*10000));
				System.out.println(getCurrThreadName()+" ready to swap payload "+p);
				p = ex.exchange(p);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
